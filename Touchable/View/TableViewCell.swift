//
//  TableViewCell.swift
//  Touchable
//
//  Created by Pham Duc Hieu on 12/11/18.
//  Copyright © 2018 Pham Duc Hieu. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var lbCount: UILabel!
    @IBOutlet var imgDefault: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
