//
//  CheckTouchableView.swift
//  Touchable
//
//  Created by Pham Duc Hieu on 12/12/18.
//  Copyright © 2018 Pham Duc Hieu. All rights reserved.
//

import UIKit

class CheckTouchableView: UIView {

    var touchViews = [UITouch:TouchSpotView]()
    var Tches: Set<UITouch> = []
    var PointList = [PointData]()
    var listD: [Float] = []
    var sortedList: [Float] = []
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isMultipleTouchEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        isMultipleTouchEnabled = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            createViewForTouch(touch: touch)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let view = viewForTouch(touch: touch)
            // Move the view to the new location.
            let newLocation = touch.location(in: self)
            view?.center = newLocation
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        Tches = touches
        //test 2 points
        if touches.count == 5{
            for touch in touches {
                let point = touch.location(in: self)
                let aPoint  = PointData(x: Float(point.x), y: Float(point.y))
                PointList.append(aPoint)
            }
            exec()
        }
        else{
        print("Not matched!")
            for touch in touches{
                removeViewForTouch(touch: touch)
            }
        }
        
    }
    
    var touch = UITouch()
    
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            removeViewForTouch(touch: touch)
            PointList = []
            listD = []
            sortedList = []
        }
    }
    
    func createViewForTouch( touch : UITouch ) {
        let newView = TouchSpotView()
        newView.bounds = CGRect(x: 0, y: 0, width: 0, height: 0)
        newView.center = touch.location(in: self)
        
        // Add the view and animate it to a new size.
        addSubview(newView)
        UIView.animate(withDuration: 0) {
            newView.bounds.size = CGSize(width: 25, height: 25)
        }
        
        // Save the views internally
        touchViews[touch] = newView
    }
    
    func viewForTouch (touch : UITouch) -> TouchSpotView? {
        return touchViews[touch]
    }
    
    func removeViewForTouch (touch : UITouch ) {
        if let view = touchViews[touch] {
            view.removeFromSuperview()
            touchViews.removeValue(forKey: touch)
        }
    }
    
//    func removeTouches(){
//        for touch in Tches{
//            removeViewForTouch(touch: touch)
//        }
//        PointList = []
//        listD = []
//        sortedList = []
//    }
    
    func SortBubble(listSort: [Float]) -> [Float]{
        var list:[Float] = listSort
        for i in 0..<listSort.count-1{
            for j in 0..<listSort.count-1-i{
                var sort:Float
                if(list[j]>list[j+1]){
                    sort = list[j]
                    list[j] = list[j+1]
                    list[j+1] = sort
                }
            }
        }
        return list
    }
    
    func exec(){
        var d: Float = 0
        for i in 0..<PointList.count{
            let x1:Float=PointList[i].x
            let y1:Float=PointList[i].y
            for j in i+1..<PointList.count{
                let x2:Float=PointList[j].x
                let y2:Float=PointList[j].y
                let powx = pow(x2-x1, 2)
                let powy = pow(y2-y1, 2)
                d = sqrt(powx+powy)
                listD.append(d)
            }
        }
        sortedList = SortBubble(listSort: listD)
        for i in sortedList{
            print(i)
        }
    }

}
