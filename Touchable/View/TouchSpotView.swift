//
//  TouchSpotView.swift
//  Touchable
//
//  Created by Pham Duc Hieu on 12/10/18.
//  Copyright © 2018 Pham Duc Hieu. All rights reserved.
//

import UIKit

class TouchSpotView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.green
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Update the corner radius when the bounds change.
    override var bounds: CGRect {
        get { return super.bounds }
        set(newBounds) {
            super.bounds = newBounds
            layer.cornerRadius = newBounds.size.width / 2.0
        }
    }

}
