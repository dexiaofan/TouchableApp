//
//  RegisterSignController.swift
//  Touchable
//
//  Created by Pham Duc Hieu on 12/10/18.
//  Copyright © 2018 Pham Duc Hieu. All rights reserved.
//

import UIKit
protocol DoneDelegate {
    func getData(list: [Float])
}
class RegisterSignController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    var delegate: DoneDelegate?
    @IBOutlet weak var TView: TouchableView!
    
    @IBAction func ResetData(_ sender: Any) {
        TView.isMultipleTouchEnabled = true
        TView.removeTouches()
    }
    
    @IBAction func ExecDataDone(_ sender: Any) {
        delegate?.getData(list: TView.sortedList)
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
