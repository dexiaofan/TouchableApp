//
//  ViewController.swift
//  Touchable
//
//  Created by Pham Duc Hieu on 12/10/18.
//  Copyright © 2018 Pham Duc Hieu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tblResult: UITableView!
    
//    var listResult:[Float] = []
    var listAllResult:[Sign] = []
    var aSign = Sign(signCount: 0, img: "", resultList: [])
    override func viewDidLoad() {
        super.viewDidLoad()
        tblResult.dataSource = self
        tblResult.delegate = self
    }

    @IBAction func RegisterSign(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let regSignSb = sb.instantiateViewController(withIdentifier: "RegisterSignController") as! RegisterSignController
            regSignSb.delegate=self
        self.navigationController?.pushViewController(regSignSb, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listAllResult.count < 2 || listAllResult.isEmpty{
            return 1
        }
        else{
            return listAllResult.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellRegis") as! TableViewCell
        if  listAllResult.isEmpty{
            cell.imgDefault.image = UIImage(named: "default")
            cell.lbCount.text = "No Sign"
        } else{
            cell.imgDefault.image = UIImage(named: listAllResult[indexPath.row].img)
            cell.lbCount.text = String(listAllResult[indexPath.row].signCount) + "/10"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let checkSign = sb.instantiateViewController(withIdentifier: "checkSign") as! CheckSignController
        
       
       if !listAllResult.isEmpty{
        checkSign.signCount = listAllResult[indexPath.row].signCount
        checkSign.sign = listAllResult[indexPath.row].resultList
        self.navigationController?.pushViewController(checkSign, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}
extension ViewController: DoneDelegate{
    func getData(list: [Float]) {
//        listResult = list
        aSign = Sign(signCount: 0, img: "1", resultList: list)
        if !list.isEmpty{
            listAllResult.append(aSign)
        }
        tblResult.reloadData()
    }
}

