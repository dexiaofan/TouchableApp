//
//  CheckSignController.swift
//  Touchable
//
//  Created by Pham Duc Hieu on 12/12/18.
//  Copyright © 2018 Pham Duc Hieu. All rights reserved.
//

import UIKit
protocol CheckDoneDelegate {
    func getData(count: Int)
}
class CheckSignController: UIViewController {

    var delegate: CheckDoneDelegate?
    var signCount:Int = 0
    var sign:[Float] = []
    var isValid:[Bool] = []
    @IBOutlet weak var lbMsgCount: UILabel!
    @IBOutlet weak var CheckView: CheckTouchableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    @IBAction func CheckDone(_ sender: Any) {
        let checkValue:[Float] = CheckView.sortedList
        if checkValue.isEmpty{
            print("Nothing")
        }else{
            for i in 0..<sign.count{
                let minASign = sign[i] - 10
                let maxASign = sign[i] + 10
                print("\(maxASign) + \(minASign)")
                if checkValue[i] >= minASign && checkValue[i] <= maxASign{
                    print("\(checkValue[i]) between \(minASign) and \(maxASign)")
                    isValid.append(true)
                }else{
                    isValid.append(false)
                }
                if isValid.contains(false){
                    print("Not matched!")
                }else{
                    print(sign)
                    print(isValid)
                    print(checkValue)
                    print("Accumulate points successfully!")
                    signCount += 1
                    CheckView.isMultipleTouchEnabled = false
                    delegate?.getData(count: signCount)
                    self.navigationController?.popViewController(animated: true)
                }
            }

        }
    }
    
}
