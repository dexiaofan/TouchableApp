//
//  Sign.swift
//  Touchable
//
//  Created by Pham Duc Hieu on 12/11/18.
//  Copyright © 2018 Pham Duc Hieu. All rights reserved.
//

import Foundation
class Sign {
    public var signCount: Int = 0
    public var img:String
    public var resultList:[Float] = []
    
    init(signCount: Int, img: String, resultList:[Float]) {
        self.signCount = signCount
        self.img = img
        self.resultList = resultList
    }
}
