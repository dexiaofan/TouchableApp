//
//  PointData.swift
//  Touchable
//
//  Created by Pham Duc Hieu on 12/10/18.
//  Copyright © 2018 Pham Duc Hieu. All rights reserved.
//

import Foundation
class PointData{
    var x:Float = 0
    var y:Float = 0
    
    init(x:Float, y:Float) {
        self.x = x
        self.y = y
    }
}
